IMAGE_NAME=panic
REGISTRY=dvision
VERSION=1.0.0

build:
	go build -o ./bin/${IMAGE_NAME} ./cmd/main.go

image:
	docker build --no-cache  -t $(REGISTRY)/${IMAGE_NAME}:$(VERSION) -f ./Dockerfile . 

run: build
	./bin/$(IMAGE_NAME)

image_run:
	docker run -p 8080:8080 dvision/$(IMAGE_NAME):1.0.0
