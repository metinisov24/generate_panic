package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/labstack/echo"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func main() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	e := echo.New()
	//e.Use(middleware.Recover())
	e.GET("/panic", func(c echo.Context) error {

		log.Debug().
			Str("Scale", "833 cents").
			Float64("Interval", 833.09).
			Msg("Fibonacci is everywhere")

		panic("This is a panic...")
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.Logger.Fatal(e.Start(":8087"))
}

func structuredPanic(out io.Writer, i interface{}, time string, stack []byte) {
	bytes, err := json.Marshal(struct {
		Level   string `json:"level"`
		Time    string `json:"time"`
		Message string `json:"msg"`
		Stack   string `json:"stack"`
	}{
		Level:   "fatal",
		Time:    time,
		Message: fmt.Sprintf("%v", i),
		Stack:   string(stack),
	})
	if err != nil {
		fmt.Fprintf(out, "error while serializing cmd exit panic: %+v\n", err) // nolint: errcheck
		fmt.Fprintf(out, "original panic: %+v\n", i)                           // nolint: errcheck
		return
	}
	fmt.Fprintf(out, "%s\n", bytes) // nolint: errcheck
}
