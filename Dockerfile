FROM golang:1.14.3-alpine

RUN apk add --no-cache git libgit2-dev alpine-sdk

WORKDIR /go/src/gitlab.com/metinisov24/generate_panic

# https://divan.github.io/posts/go_get_private/
COPY .gitconfig /root/.gitconfig
COPY go.mod .
COPY go.sum .
ENV GOPRIVATE=gitlab.com/dvision,gitlab.com/miapago,gitlab.com/metinisov24
# install dependencies
RUN go mod download

COPY ./cmd/ ./cmd/

RUN go build -o ./bin/panic ./cmd/

RUN go get gitlab.com/metinisov24/panic_trace/v2

RUN go build -o ./bin/panicparce gitlab.com/metinisov24/panic_trace/v2

FROM alpine:latest

WORKDIR /go/src/panic

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
# RUN apk add --no-cache git libgit2-dev alpine-sdk
RUN apk --no-cache add curl


COPY --from=0 /go/src/gitlab.com/metinisov24/generate_panic/bin/panic .

COPY --from=0 /go/src/gitlab.com/metinisov24/generate_panic/bin/panicparce .

RUN ls -ltr

EXPOSE 8080

CMD [ "sh", "-c", "./panic | ./panicparce" ]
#CMD ["./panic |&./panic_trace"]
